package ProblemSolving;

import java.util.Scanner;

public class Temperature {

public static void main(String [] args) {
		
		//standard input
		Scanner in = new Scanner(System.in);
		System.out.print("how time Input of temerature level : ");
		int num = in.nextInt();
		
		//Temperature is 0
		if(num <= 0) {
			System.out.println(0); 
			in.close();
			return;
		}
		
		//find the last number is the zero
		int last = 0;
		for(int i = 0; i < num; i++) {
			int nowtem = in.nextInt();
			if(last == 0 || Math.abs(last) > Math.abs(nowtem) 
			|| Math.abs(last) == Math.abs(nowtem) && last < nowtem) last = nowtem;
		}
		System.out.println("Dislay the temerature closest to 0 : "+last);
		in.close();
	}
}
