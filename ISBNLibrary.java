package ProblemSolving;

import java.io.IOException;
import java.util.Scanner;

public class ISBNLibrary {

	  public static void main(String[] args) throws IOException {
	        int s = 0, i, t, d, dNumber;
	        String st;
	        
	        // Input a 10-digit ISBN number
	         
	    	Scanner in = new Scanner(System.in);
	        System.out.print("Input 10 digits of ISBN number: ");
	        long isbnNumber1 = in.nextInt();

	         
	        // check the length is 10
	        st = "" + isbnNumber1;
	        if (st.length() != 10) {
	            System.out.println("Wrong ISBN number");
	            return;
	        }
	        
	         
	        s = 0;
	        for (i = 0; i < st.length(); i++) {
	            d = Integer.parseInt(st.substring(i, i + 1));
	            dNumber = i + 1;
	            t = dNumber * d;
	            s += t;
	        }

	        // check the number s is divisible by 11 or not
	      
	        if ((s % 11) != 0) {
	            System.out.println("Wrong ISBN number");
	        } else {
	            System.out.println("Correct ISBN number");
	        }
	    }
}
